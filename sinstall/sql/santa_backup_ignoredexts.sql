DROP TABLE IF EXISTS `%PREFIX%_backup_ignoredexts`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_backup_ignoredexts` (
  `ruleid` int(5) unsigned default NULL,
  `ext` varchar(255) NOT NULL,
  UNIQUE KEY `ext` (`ext`,`ruleid`),
  KEY `ruleid` (`ruleid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Исключаемые расширения файлов из бекапа';
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredexts` VALUES ('2','avi');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredexts` VALUES ('2','swf');