# Добро пожаловать в ветку разработки Santafox CMS #

### Как начать разрабатывать под Santafox CMS? ###

* [Ознакомитесь с документацией по разработке модулей ](http://wiki.santafox.ru/doku.php?id=constructor:all_modules)
* [Ознакомьтесь с рекомендуемым стилем программирования для Santafox CMS ](http://wiki.santafox.ru/doku.php?id=modules_create:style)
* [Документация доступна на сайте и на вики странице](http://www.santafox.ru/manual.html)

План тут: 
https://docs.google.com/spreadsheets/d/1jtDit3hheeSoma0CWAV8q-3_yu-fNEQxiB18StaNstM/edit#gid=0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact